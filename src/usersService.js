/* eslint-disable no-unused-vars */
/* eslint-disable no-underscore-dangle */
const jwt = require('jsonwebtoken');
const bcryptjs = require('bcryptjs');
const dotenv = require('dotenv').config();

const { User } = require('./models/Users');

const sekretKey = process.env.SECRET_KEY;

const registerUser = async (req, res, next) => {
  const { name, username, password } = req.body;

  const user = new User({
    name,
    username,
    password: await bcryptjs.hash(password, 10),
  });
  user.save()
    .then((saved) => res.status(200).send({ message: 'Success' }))
    .catch((err) => {
      res.status(400).send({ message: err.message });
    });
};

const loginUser = async (req, res, next) => {
  const user = await User.findOne({ username: req.body.username });
  if (!user) {
    return res.status(400).json({ message: 'User wasn\'t found' });
  }
  if (user && await bcryptjs.compare(String(req.body.password), String(user.password))) {
    const payload = {
      username: user.username,
      name: user.name,
      userId: user._id,
    };
    const jwtToken = jwt.sign(payload, sekretKey);
    return res.json({
      message: 'Success',
      jwt_token: jwtToken,
    });
  }
  return res.status(403).json({ message: 'Not authorized' });
};

const getProfileInfo = async (req, res, next) => {
  const user = await User.findOne({ username: req.user.username });
  console.log(user);
  if (!user) {
    return res.status(400).json({ message: 'User wasn\'t found' });
  }
  const { _id, username, createdDate } = user;
  return res.status(200).json({ user: { _id, username, createdDate } });
};

const deleteProfile = async (req, res, next) => {
  const user = await User.findOneAndDelete({ username: req.user.username });
  if (!user) {
    return res.status(400).json({ message: 'User wasn\'t found' });
  }
  return res.status(200).json({ message: 'Success' });
};

const changeProfilePassword = async (req, res, next) => {
  const { oldPassword, newPassword } = req.body;
  if (!oldPassword || !newPassword) {
    return res.status(400).json({ message: 'Please enter old and new passwords' });
  }
  if (!User.findOne({ username: req.user.username })) {
    return res.status(400).json({ message: 'User wasn\'t found' });
  }
  const hashedPassword = await bcryptjs.hash(newPassword, 10);
  return User.findOneAndUpdate(
    { username: req.user.username },
    { $set: { password: hashedPassword } },
  ).then(() => {
    res.json({ message: 'Success' });
  });
};
module.exports = {
  registerUser,
  loginUser,
  getProfileInfo,
  deleteProfile,
  changeProfilePassword,
};

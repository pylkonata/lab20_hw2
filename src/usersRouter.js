const express = require('express');

const router = express.Router();
const {
  registerUser, loginUser, getProfileInfo, deleteProfile,
  changeProfilePassword,
} = require('./usersService');
const { authMiddleware } = require('./middleware/authMiddleware');

router.post('/auth/register', registerUser);
router.post('/auth/login', loginUser);
router.get('/users/me', authMiddleware, getProfileInfo);
router.delete('/users/me', authMiddleware, deleteProfile);
router.patch('/users/me', authMiddleware, changeProfilePassword);

module.exports = {
  usersRouter: router,
};

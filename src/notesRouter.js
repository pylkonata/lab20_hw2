const express = require('express');

const router = express.Router();
const {
  createNote, getNotes, getNoteById, updateNoteById,
  changeNoteCompletedById, deleteNoteById,
} = require('./notesService');
const { authMiddleware } = require('./middleware/authMiddleware');

router.post('/', authMiddleware, createNote);

router.get('/', authMiddleware, getNotes);

router.get('/:id', authMiddleware, getNoteById);

router.put('/:id', authMiddleware, updateNoteById);

router.patch('/:id', authMiddleware, changeNoteCompletedById);

router.delete('/:id', authMiddleware, deleteNoteById);

module.exports = {
  notesRouter: router,
};

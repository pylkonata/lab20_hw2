/* eslint-disable no-unused-vars */
const { Note } = require('./models/Notes');
const { User } = require('./models/Users');

// eslint-disable-next-line consistent-return
function createNote(req, res, next) {
  const { text } = req.body;
  if (!text) {
    return res.status(400).json({ message: 'Please enter note' });
  }
  const { userId } = req.user;
  const note = new Note({
    userId,
    text,
  });

  note.save().then((saved) => res.status(200).json({ message: 'Success' }));
}

const getNotes = async (req, res) => {
  const { userId } = req.user;
  const notes = await Note.find({ userId });
  const { offset = 0, limit = notes.length } = req.query;
  const end = (notes.length - offset - limit) <= 0 ? notes.length
    : +offset + +limit;
  const selected = notes.slice(offset, end);
  return res.status(200).json({
    offset,
    limit,
    count: notes.length,
    notes: selected,
  });
};

const getNoteById = async (req, res) => {
  try {
    const { userId } = req.user;
    const note = await Note.findById(req.params.id);
    if (!note || userId !== note.userId.toString()) {
      return res.status(400).json({ message: 'Note not found' });
    }
    return res.status(200).json({ note });
  } catch (err) {
    throw err.message;
  }
};

const updateNoteById = async (req, res) => {
  const note = await Note.findById(req.params.id);
  const { userId } = req.user;
  if (!note || userId !== note.userId.toString()) {
    return res.status(400).json({ message: 'Note not found' });
  }
  const { text } = req.body;

  if (text) {
    note.text = text;
  } else {
    return res.status(200).json({ message: 'Please enter new text for note' });
  }
  return note.save().then(() => res.status(200).json({ message: 'Success' }));
};

const changeNoteCompletedById = async (req, res) => {
  const note = await Note.findById(req.params.id);
  const { userId } = req.user;
  if (!note || userId !== note.userId.toString()) {
    return res.status(400).json({ message: 'Note not found' });
  }
  note.completed = !note.completed;
  return note.save().then(() => res.status(200).json({ message: 'Success' }));
};

const deleteNoteById = async (req, res) => {
  const { userId } = req.user;
  const note = await Note.findById(req.params.id);
  if (!note || userId !== note.userId.toString()) {
    return res.status(400).json({ message: 'Note not found' });
  }
  return Note.findByIdAndDelete(req.params.id)
    .then(() => res.status(200).json({ message: 'Success' }));
};

module.exports = {
  createNote,
  getNotes,
  getNoteById,
  updateNoteById,
  changeNoteCompletedById,
  deleteNoteById,
};

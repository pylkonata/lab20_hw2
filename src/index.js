/* eslint-disable no-unused-vars */
const fs = require('fs');
const express = require('express');
const morgan = require('morgan');
const cors = require('cors');

const app = express();
const mongoose = require('mongoose');

mongoose.connect('mongodb+srv://pylkonata:Gn5%255GuBufW7-qe@cluster0.0x11au7.mongodb.net/todoapp?retryWrites=true&w=majority');

const PORT = 8080;

const { notesRouter } = require('./notesRouter');
const { usersRouter } = require('./usersRouter');

app.use(express.json());
app.use(morgan('tiny'));
app.use(cors());
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', req.headers.origin);
  res.header('Access-Control-Allow-Credentials', 'true');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
  next();
});

app.use('/api/notes', notesRouter);
app.use('/api', usersRouter);

const start = async () => {
  try {
    app.listen(PORT);
  } catch (err) {
    console.error(`Error on server startup: ${err.message}`);
  }
};

start();

// ERROR HANDLER

function errorHandler(err, req, res, next) {
  console.error(err);
  res.status(500).send({ message: 'Server error' });
}
app.use(errorHandler);
